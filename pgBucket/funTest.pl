#!/usr/bin/perl
#
# pgBucket functionality test cases
# 
#
#
$ENV{'PGUSER'} = 'postgres';
$ENV{'PGDATABASE'} = 'postgres';
$ENV{'PGPORT'} = '5432';
$ENV{'PGPASSWORD'} = 'postgres';
$ENV{'PGBUCKET_SOCK_DIR'} = '/tmp/';
$ENV{'PGBUCKET_PID_DIR'} = '/tmp/';
$ENV{'PGHOSTADDR'} = '127.0.0.1';

sub PrintTestHeader
{
	print "==========================\n";
	print "Testing ", $_[0], "\n";
	print "==========================\n";
}

sub PrintError
{
	print "##########################\n";
	print "ERROR: ", $_[0], "\n";
	print "##########################\n";
}

sub PrintStatus
{
	print "..........................\n";
	print "Status: ", $_[0], "\n";
	print "..........................\n";
}

sub TestPrintHelp
{
	PrintTestHeader("help -h");
	$output = `./pgBucket -h 2>&1`;
	$error =  `./pgBucket -h 2>&1 1>/dev/null`;
	print "Ouput:\n", $output;
	
	if (length($error) > 0) {
		PrintError($error);
		PrintStatus("Failed");
	} else {
		PrintStatus("OK");
	}
}

sub TestPgBucketInit
{
	PrintTestHeader("init -I");
	$output = `./pgBucket -I 2>&1`;
	print "Output:\n", $output;

	if ($output =~ 'pgBucket catalog is created') {
		PrintStatus("OK");
	} else {
		PrintError("pgBucket catalog creation is failed");
	}
}

TestPrintHelp();
TestPgBucketInit();
