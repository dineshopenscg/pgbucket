/*
 * pgBucket job object declaration
 *
 * Author:
 *      Dinesh Kumar dineshkumar02@gmail.com
 *
 * LOCATION
 *		pgBucket/include/jobs.h
 */

#ifndef INCLUDE_JOBS_H_
#define INCLUDE_JOBS_H_

constexpr auto OS_RESULT_BUFSIZE = 256;
constexpr auto READ = 0;
constexpr auto WRITE = 1;

#include "libpq-fe.h"
#include <errno.h>
#include <unistd.h>
#include <sys/wait.h>
#include <spawn.h>
#include "utils/utils.h"

enum jobClass {
    JOB, EVENT, UNKNOWN_CLASS
};

enum jobTypes {
    OSLEVEL, DBLEVEL, UNKNOWN_TYPE
};

enum jobSchStatus {
    INITIALIZED, SCHEDULED, RUNNING, RUNNING_EVENTJOB, DISPATCHED, COMPLETED, SKIPPED, KILLED
};

enum osRunKind {
    FORK_EXEC, POSIX_SPAWN
};

class jobs;

class schEntityPref
{
    private:
        size_t jobID;
        string jobName;
        bool jobStatus; /*Active/In Active */
        size_t nTimesRun;
        time_t jobStartTime;
        time_t jobEndTime;
        bool jobPrevRunStatus;
        jobTypes jobType;
        jobClass jClass;
        mutex jobLock;
        jobSchStatus schStatus;
        pid_t jobPid;
        bool isSuccess;	// Execution status
        int disableFailCnt; // Job's event loop
        int faileCnt;
        string jobFailIfOutput;
        string jobCmd;
        string result;
        string error;
        bool isDeleted;
        string jobDbConnStr;
        size_t *peJobIds; /*Pass job event ids*/
        size_t *feJobIds; /*Fail job event ids*/
        bool isRecordDbResultColNames;
        bool isParseCmdParams;
    public:
        schEntityPref();
        schEntityPref (size_t id, string &name, bool jobstatus, jobTypes jobtype, jobClass jClass, string &jobcmd, string &conStr, string &jobFailIfOut,
                       size_t *pjids, size_t *fjids, int disableFailCnt, bool isStoreDBCols, bool isParseCmd);

        void setJobName (string name);
        void setJobStatus (bool status);
        void setJobType (jobTypes type);
        jobTypes getJobType();
        jobClass getJobClass() const;
        size_t getJobID();
        string getJobName();
        bool getJobStatus();
        time_t getJobStartTime();
        size_t getJobRunCounter();
        void incJobRunCounter();
        size_t *getNTimesRun();
        void lockJob();
        void unlockJob();
        void tryLock();
        jobSchStatus getJobSchStatus();
        void setJobSchStatus (jobSchStatus status);
        void setJobPid (pid_t pid);
        pid_t getJobPid();
        void setPrevRunStatus (bool val);
        bool getPrevRunStatus();
        time_t getJobEndTime();
        void setJobStartTime (time_t val);
        void setJobEndTime (time_t val);
        void setJobSuccess (const bool status);
        bool getJobSuccess() const;
        void setJobClass (jobClass jType);
        int getDisableFailCnt() const;
        void setDisableFailCnt (int dcnt);
        string getJobFailIfOutput() const;
        void setJobFailIfOutput (const string &jobFailIfOut);
        string getJobCmd();
        void setJobCmd (string cmd);
        bool isJobDeleted();
        void setJobDelete (bool val);
        string getDbConnStr();
        void setDbConnStr (string connstr);
        void setJobFailEvtIds (size_t *fjids);
        size_t *getJobFailEvtIds() const;
        void setJobPassEvtIds (size_t *pjids);
        size_t *getJobPassEvtIds() const;
        void setJobResult (const string &res);
        string getJobResult() const;
        void setJobError (const string &err);
        string getJobError() const;
        void initJobSettings();
        void passJobSettings();
        void failJobSettings();
        bool getIsParseCmdParams() const;
        void setIsParseCmdParams (bool val);
        bool getIsRecordDbResCols() const;
        void setIsRecordDbResCols (bool val);
        string getSchStatusStr() const;
        int getJobFailCnt() const;
        void resetJobFailCounter();
        void incJobFailCounter();
        ~schEntityPref();
};

class jobPref : public schEntityPref
{

    private:
        vector<int> *hrs;
        vector<int> *mns;
        vector<int> *secs;
        int hrsPntPos;
        int mnsPntPos;
        int secPntPos;
        time_t jobNextRun;
        bool skipNextRun;
        string execPath;
    public:
        jobPref();
        jobPref (size_t id, string &name, bool jobstatus, vector<int> *hs, vector<int> *ms, vector<int> *ss,
                 jobTypes jobtype, string &jobcmd, string &conStr, jobClass jClass, size_t *passjids, size_t *failjids,
                 string &jobFailIfOut, int disableFailCnt, bool isStoreDBCols, bool isParseCmd);

        void setJobNextRun (time_t val);
        time_t getJobNextRun();
        void setJobHrsPntPos (int p);
        void setJobMnsPntPos (int p);
        void setJobSecPntPos (int p);
        int getJobHrsPntPos();
        int getJobMnsPntPos();
        int getJobSecPntPos();
        vector<int> *getJobHrs();
        vector<int> *getJobMns();
        vector<int> *getJobSecs();
        void setJobHrs (vector<int> *);
        void setJobMns (vector<int> *);
        void setJobSecs (vector<int> *);
        void setJobSkipNextRun (bool val);
        void setExecPath (const string &ePath);
        const string getExecPath();
        bool getJobSkipNextRun();
        virtual ~jobPref();
};

class jobs: public jobPref
{
    public:
        jobs();
        jobs (size_t id, string &name, bool jobstatus, vector<int> *hs, vector<int> *ms, vector<int> *ss,
              jobTypes jobtype, string &jobcmd, string &conStr, jobClass jClass, size_t *passjids, size_t *failjids,
              string &jobFailIfOut, int disableFailCnt, bool isStoreDBCols, bool isParseCmd);

        virtual bool runJob (string &result, string &err, pid_t &pid) = 0;
        virtual void killJob (bool isForce = false) = 0;
        ~jobs();
};

class osJobs: public jobs
{
    private:
        char *cmdResultBuf;
        int *fd;
        osRunKind oRKind;
    public:
        osJobs();
        osJobs (size_t id, std::string &name, bool jobstatus, vector<int> *hs, vector<int> *ms, vector<int> *ss,
                jobTypes jobtype, string &cmd, osRunKind rKind, string &conStr, jobClass jClass, size_t *passjids, size_t *failjids, string &jobFailIfOut, int disableFailCnt, bool isStoreDBCols, bool isParseCmd);
        virtual ~osJobs();
        bool runJob (string &result, string &err, pid_t &pid);
        void killJob (bool isForce = false);
        bool execCmd (string cmd, int &eno, pid_t &pid);
        osRunKind getOSRunKind() const;
};

class dbJobs: public jobs
{
    public:
        dbJobs();
        dbJobs (size_t id, string &name, bool jobstatus, vector<int> *hs, vector<int> *ms, vector<int> *ss,
                jobTypes jobtype, string &jobCmd, string &connStr, jobClass jClass, size_t *passjids, size_t *failjids, string &jobFailIfOut, int disableFailCnt, bool isStoreDBCols, bool isParseCmd);
        virtual ~dbJobs() {};
        bool runJob (string &result, string &err, pid_t &pid);
        void killJob (bool isForce = false);
};
#endif /* INCLUDE_JOBS_H_ */
